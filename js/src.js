var scene,camera,renderer;
var ultiTiempo;
var labels = [];
var objetos = {};
var appW = window.innerWidth;
var appH = window.innerHeight;
var clock;

function webGLStart(){

	clock = new THREE.Clock;

	iniciarEscena();
	llenarEscena();
	ultiTiempo = Date.now();
	animarEscena();
}

function iniciarEscena(){
	
	//RENDERER
	renderer = new THREE.WebGLRenderer( {antialias: true} );
	renderer.setSize( appW , appH);
	renderer.setClearColorHex( 0xAAAAAA, 1.0);

	renderer.shadowMapEnabled = true;

	document.body.appendChild(renderer.domElement);

	//CAMARAS
	var view_angle = 45, aspect_ratio = appW/appH, near = 1, far = 20000;
	camera = new THREE.PerspectiveCamera( view_angle, aspect_ratio, near, far);
	camera.position.set( 0,150,400 );

	//CONTROL DE LA CAMARA
	controlCamara = new THREE.OrbitControls( camera , renderer.domElement);

	//ESTADÍSTICAS
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '10px';
	stats.domElement.style.zIndex = '100';
	document.body.appendChild( stats.domElement );

	//CANVAS GUI
	initGUI();

	//LUZ
	ambientLight = new THREE.AmbientLight( 0xffffff );

	luzSpotLight = new THREE.SpotLight(0xffffff);
	luzSpotLight.position.set(0,400,150);

	//configuraciones adicionales de la luz para su intensidad y sombreado
	luzSpotLight.shadowCameraVisible = true; // Guias de luz

	luzSpotLight.shadowDarkness = 0.6; // Opacidad de sombras
	luzSpotLight.intensity = 2.5; // Intensidad de Luz
	luzSpotLight.castShadow = true; // Castea Sombras?

	//Shadow map texture width in pixels.
	luzSpotLight.shadowMapWidth = luzSpotLight.shadowMapHeight = 512;

	luzSpotLight.shadowCameraNear = 100;
	luzSpotLight.shadowCameraFar = 650;
	luzSpotLight.shadowCameraFov = 60;

	// BOMBILLAs
	bombilla = new THREE.Mesh(
		new THREE.SphereGeometry( 10, 16, 8),
		new THREE.MeshBasicMaterial( {color: 0xffaa00, wireframe: true})
		);

	// SUELO
	var floorTexture = new THREE.ImageUtils.loadTexture( 'textures/floor-texturedundjinni-mapping-software---forums--stone-floor-texture-uel7wdrx.jpg');

	floorTexture.wrapS = floorTexture.wrapT = THREE.MirroredRepeatWrapping;
	//floorTexture.wrapS = THREE.ClampToEdgeWrapping;
	//floorTexture.wrapT = THREE.MirroredRepeatWrapping;

	floorTexture.repeat.set( 10, 10 );
	var floorMaterial = new THREE.MeshBasicMaterial( { map: floorTexture, side: THREE.DoubleSide } );
	var floorGeometry = new THREE.PlaneGeometry(1000, 1000, 10, 10);
	floor = new THREE.Mesh(floorGeometry, floorMaterial);
	//Se baja el suelo 0.5 unidades para que los objetos se situen sobre el
	floor.position.y = -0.5;
	//Aplicar la rotación para acostar el suelo
	floor.rotation.x = Math.PI / 2;

	//Activamos la recepción de sombras sobre el suelo, para esto tambien se debe activar la emisión de sombras de los elementos
	floor.receiveShadow = true;

	//Esferas
	var sphereGeom =  new THREE.SphereGeometry( 50, 32, 16 );
	
	// Materiales oscuros MeshBasic / MeshLambert / MeshPhong
	var darkMaterial = new THREE.MeshBasicMaterial( { color: 0x000088, transparent:true, opacity:1 } );
	var darkMaterialL = new THREE.MeshLambertMaterial( { color: 0x000088, transparent:true, opacity:1 } );
	var darkMaterialP = new THREE.MeshPhongMaterial( { color: 0x000088, transparent:true, opacity:1 } );
	
	// Mallas de esfera
	var sphere = new THREE.Mesh( sphereGeom.clone(), darkMaterial );
	sphere.position.set(-150, 50, 0);
	objetos.sphere1= sphere;	
	
	var sphere = new THREE.Mesh( sphereGeom.clone(), darkMaterialL );
	sphere.position.set(0, 50, 0);
	objetos.sphere2= sphere;	
	
	var sphere = new THREE.Mesh( sphereGeom.clone(), darkMaterialP );
	sphere.position.set(150, 50, 0);
	objetos.sphere3= sphere;

	sonido1 = new Sound(['sounds/1.mp3'],250,1);
	sonido1.position.set(-250,0,-250);
	sonido1.play();

	sonido2 = new Sound(['sounds/2.mp3'],250,1);
	sonido2.position.set(250,0,-250);
	sonido2.play();

	sonido3 = new Sound(['sounds/3.mp3'],250,1);
	sonido3.position.set(-250,0,250);
	sonido3.play();

	sonido4 = new Sound(['sounds/4.mp3'],250,1);
	sonido4.position.set(250,0,250);
	sonido4.play();

}

function llenarEscena() {
	scene = new THREE.Scene();
	scene.fog = new THREE.Fog( 0x808080, 1000, 6000 );
	scene.add( camera );
	camera.lookAt(scene.position);

	// Luces
	//Sin la luz ambiente no se perciben las componentes ambiente de los elementos, estas por defecto son blancas y para fines de práctica se adicionará esta luz al final del taller.
	scene.add( ambientLight );
	scene.add( luzSpotLight );
	scene.add( bombilla );
	bombilla.position = luzSpotLight.position;

	//Elementos
	scene.add(floor);

	for(var i=0; i<Object.keys(objetos).length; i++)
	{
		var object = Object.keys(objetos)[i];
		objetos[object].castShadow = true;
		objetos[object].shininess = 30;
		scene.add(objetos[object]);
	}

}

function animarEscena(){
	requestAnimationFrame( animarEscena );
	renderEscena();
	actualizarEscena();
}

function renderEscena(){
	renderer.render( scene, camera );
}

function actualizarEscena(){
	
	ultiTiempo = Date.now();
	controlCamara.update();

	var time = Date.now() *0.0005;
	var delta = clock.getDelta();

	luzSpotLight.position.x = Math.sin( time * 1.4 ) * 120;
	luzSpotLight.position.y = 500+Math.cos( time * 0.5 ) * 80;
	luzSpotLight.position.z = 150+Math.cos( time * 0.3 ) * 60;

	stats.update();
	sonido1.update(camera);
	sonido2.update(camera);
	sonido3.update(camera);
	sonido4.update(camera);

}

function initGUI(){

	gui = new dat.GUI({ autoPlace: false});
	document.getElementById("DATGUI").appendChild(gui.domElement);

	parameters = 
	{
		x:0, y:0, z:0,
		color:  "#000088",
		colorA: "#ffffff",
		colorE: "#000000",
		colorS: "#a9a9a9",
		shininess: 30,
		opacity: 1,
		visible: true
	};

	var folderPos = gui.addFolder('Posición de las esferas');
	var elementsPosX = folderPos.add( parameters, 'x').min(-200).max(200).step(1).listen();
	var elementsPosY = folderPos.add( parameters, 'y').min(-200).max(200).step(1).listen();
	var elementsPosZ = folderPos.add( parameters, 'z').min(-200).max(200).step(1).listen();

	//A & R (Listeners)
	elementsPosX.onChange(function(value){
		updateElements('position','x',value);
	});
	elementsPosY.onChange(function(value){
		updateElements('position','y',value);
	});
	elementsPosZ.onChange(function(value){
		updateElements('position','z',value);
	});


	folderPos.open();

	var folderCompMaterial = gui.addFolder('componentes Material');
	var elementsColor = folderCompMaterial.addColor( parameters, 'color').name('Color (Diffuse)').listen();
	var elementsColorAmbient = folderCompMaterial.addColor( parameters, 'colorA').name('Color (Ambient)').listen();
	var elementsColorEmissive = folderCompMaterial.addColor( parameters, 'colorE').name('Color (Emissive)').listen();
	var elementsColorSpecular = folderCompMaterial.addColor( parameters, 'colorS').name('Color (Specular)').listen();
	var elementsShininess = folderCompMaterial.add( parameters, 'shininess').min(0).max(100).step(1).name("Shininess").listen();
	var elementsOpacity = folderCompMaterial.add( parameters, 'opacity').min(0).max(1).step(0.01).name("Opacity").listen();

	//A & R (Listeners)

	elementsColor.onChange(function(value){
		updateElements('color','color',value.replace("#","0x"));
	});
	elementsColorAmbient.onChange(function(value){
		updateElements('color','ambient',value.replace("#","0x"));
	});
	elementsColorEmissive.onChange(function(value){
		updateElements('color','emissive',value.replace("#","0x"));
	});
	elementsColorSpecular.onChange(function(value){
		updateElements('color','specular',value.replace("#","0x"));
	});

	elementsShininess.onChange(function(value){
		updateElements(null,'shininess',value);
	});
	elementsOpacity.onChange(function(value){
		updateElements(null,'opacity',value);
	});

	folderCompMaterial.open();

	

}

function updateElements(option,property,value){
	properties = (property.split(".").length > 1)? property.split(".") : property;


	for (var i = 0; i < Object.keys(objetos).length; i++) {
		
		var objeto = Object.keys(objetos)[i];

		switch(option){
			case "position":
			if(property == "x"){
				objetos[objeto].position[property] = value + ((i*150) - 150);
			}else{
				objetos[objeto].position[property] = value;
			}
			break;
			case "color":
			var basicMaterial = objetos[objeto].material instanceof THREE.MeshBasicMaterial;
			if(!basicMaterial){
				var lambertMaterial = objetos[objeto].material instanceof THREE.MeshLambertMaterial;
				if(!lambertMaterial){
					objetos[objeto].material[property].setHex(value);
				}else{
					if(property != "specular"){
						objetos[objeto].material[property].setHex(value);
					}
				}
			}else{
				if(property == "color"){
					objetos[objeto].material[property].setHex(value);
				}
			}
			break;
			default:
			objetos[objeto].material[property] = value;
		}

	}

}